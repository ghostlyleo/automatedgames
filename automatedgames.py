import pyautogui
import sys


if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print("You need to specify a game.")
    else:
        print("You chose: " + str(sys.argv[1]))
